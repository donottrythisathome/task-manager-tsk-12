package com.ushakov.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void removeTaskById();

    void removeTaskByName();

    void removeTaskByIndex();

    void findTaskById();

    void findTaskByName();

    void findTaskByIndex();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskById();

    void startTaskByName();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByName();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByName();

    void changeTaskStatusByIndex();

}
