package com.ushakov.tm.api;

import com.ushakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
