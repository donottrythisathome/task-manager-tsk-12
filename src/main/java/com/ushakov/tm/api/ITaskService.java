package com.ushakov.tm.api;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task removeOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task findOneByName(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskById(String id, String name, String description);

    Task startTaskById(String id);

    Task startTaskByName(String name);

    Task startTaskByIndex(Integer index);

    Task completeTaskById(String id);

    Task completeTaskByName(String name);

    Task completeTaskByIndex(Integer index);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByName(String name, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
