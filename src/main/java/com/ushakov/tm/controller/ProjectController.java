package com.ushakov.tm.controller;

import com.ushakov.tm.api.IProjectController;
import com.ushakov.tm.api.IProjectService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK] \n");
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String projectDescription = TerminalUtil.nextLine();
        final Project project = projectService.add(projectName, projectDescription);
        if (project == null) {
            System.out.println("[FAILED] \n");
            return;
        }
        System.out.println("[OK] \n");
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK] \n");
    }

    @Override
    public void removeProjectById() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(projectId);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
        System.out.println();
    }

    @Override
    public void removeProjectByName() {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(projectName);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
        System.out.println();
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        if (projectIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Project project = projectService.removeOneByIndex(projectIndex - 1);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
    }

    @Override
    public void findProjectById() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(projectId);
        if (project == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(project);
        System.out.println();
    }

    @Override
    public void findProjectByName() {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(projectName);
        if (project == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(project);
        System.out.println();
    }

    @Override
    public void findProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        if (projectIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Project project = projectService.findOneByIndex(projectIndex - 1);
        if (project == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(project);
        System.out.println();
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        if (projectIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Project project = projectService.findOneByIndex(projectIndex - 1);
        if (project == null) {
            System.out.println("PROJECT NOT FOUND \n");
            return;
        }
        System.out.println("ENTER NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String projectDescription = TerminalUtil.nextLine();
        if (projectService.updateProjectByIndex(projectIndex - 1, projectName, projectDescription) == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY UPDATED \n");
    }

    @Override
    public void updateProjectById() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(projectId);
        if (project == null) {
            System.out.println("PROJECT NOT FOUND \n");
            return;
        }
        System.out.println("ENTER NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String projectDescription = TerminalUtil.nextLine();
        if (projectService.updateProjectById(projectId, projectName, projectDescription) == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY UPDATED \n");
    }

    @Override
    public void startProjectById() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(projectId);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY STARTED \n");
        System.out.println();
    }

    @Override
    public void startProjectByName() {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(projectName);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY STARTED \n");
        System.out.println();
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        if (projectIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Project project = projectService.startProjectByIndex(projectIndex - 1);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY STARTED \n");
    }

    @Override
    public void completeProjectById() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.completeProjectById(projectId);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY COMPLETED \n");
        System.out.println();
    }

    @Override
    public void completeProjectByName() {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.completeProjectByName(projectName);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY COMPLETED \n");
        System.out.println();
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        if (projectIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Project project = projectService.completeProjectByIndex(projectIndex - 1);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY COMPLETED \n");
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusById(projectId, status);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("STATUS IS SUCCESSFULLY UPDATED \n");
        System.out.println();
    }

    @Override
    public void changeProjectStatusByName() {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusByName(projectName, status);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("STATUS IS SUCCESSFULLY UPDATED \n");
        System.out.println();
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        if (projectIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusByIndex(projectIndex-1, status);
        if (project == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("STATUS IS SUCCESSFULLY UPDATED \n");
        System.out.println();
    }

}
